from operator import is_
from rest_framework import response,status
from django.shortcuts import get_object_or_404
from .models import *
from .serializers import *
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models import Q
from rest_framework.views import APIView

class CustomPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data, status):
        return response.Response({
            'Links' : {
                'next' : self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count' : self.page.paginator.count,
            'limit' : self.page_size,
            'current_page' : self.page.number,
            'total_pages' : self.page.paginator.num_pages,
            'status_code' : status,
            'results'     : data
        })

class EmployeeListApi(APIView):
    queryset = Admin.objects.all()
    serializer_class = AdminSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['description']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Admin.objects.filter(is_active=True), pk=pk)
                emp = AdminSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Admin.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = AdminSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Admin.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = AdminSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Admin.objects.create(
                    erp_id=data.get('erp_id'),
                    mobile_number=data.get('mobile_number'),
                    Admin_name_id=data.get('Admin_name_id'),
                    role_type_id=data.get('role_type_id'),
                )
                serializer = AdminSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class RetrieveEmployeeApi(APIView):
    queryset = Admin.objects.all()
    serializer_class = AdminSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Admin.objects.filter(is_active=True), pk=pk)
                emp = AdminSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Admin.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = AdminSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Admin.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = AdminSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Admin.objects.get(id=pk)
            queryset.erp_id = data.get('erp_id')
            queryset.mobile_number = data.get('mobile_number')
            queryset.Admin_name_id = data.get('Admin_name_id')
            queryset.role_type_id = data.get('role_type_id')
            queryset.save()
            serializer = AdminSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Admin.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class QualificationtApi(APIView):
    queryset = Qualification.objects.all()
    serializer_class = QualificationSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['description']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Qualification.objects.filter(is_active=True), pk=pk)
                emp = QualificationSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Qualification.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = QualificationSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Qualification.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = QualificationSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Qualification.objects.create(
                    degree=data.get('degree'),
                    specialization=data.get('specialization'),
                    institute=data.get('institute'),
                    university=data.get('university'),
                    year_of_passing=data.get('year_of_passing'),
                    percentage=data.get('percentage'),
                    time_period=data.get('time_period'),
                    description=data.get('description'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by')
                )
                serializer = QualificationSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveQualificationApi(APIView):
    queryset = Qualification.objects.all()
    serializer_class = QualificationSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Qualification.objects.filter(is_active=True), pk=pk)
                emp = QualificationSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Qualification.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = QualificationSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Qualification.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = QualificationSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Qualification.objects.get(id=pk)
            queryset.degree = data.get('degree')
            queryset.specialization = data.get('specialization')
            queryset.institute = data.get('institute')
            queryset.university = data.get('university')
            queryset.year_of_passing = data.get('year_of_passing')
            queryset.percentage = data.get('percentage')
            queryset.time_period = data.get('time_period')
            queryset.description = data.get('description')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.save()
            serializer = QualificationSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Qualification.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class ExperienceApi(APIView):
    queryset = Experience.objects.all()
    serializer_class = ExperienceSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['description']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Experience.objects.filter(is_active=True), pk=pk)
                emp = ExperienceSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Experience.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ExperienceSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Experience.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ExperienceSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Experience.objects.create(
                    job_title=data.get('job_title'),
                    company=data.get('company'),
                    time_period=data.get('time_period'),
                    description=data.get('description'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by')
                )
                serializer = ExperienceSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveExperienceApi(APIView):
    queryset = Experience.objects.all()
    serializer_class = ExperienceSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Experience.objects.filter(is_active=True), pk=pk)
                emp = ExperienceSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Experience.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ExperienceSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Experience.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ExperienceSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Experience.objects.get(id=pk)
            queryset.job_title = data.get('job_title')
            queryset.company = data.get('company')
            queryset.time_period = data.get('time_period')
            queryset.description = data.get('description')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.save()
            serializer = ExperienceSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Experience.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class CountryApi(APIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['country_name']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Country.objects.filter(is_active=True), pk=pk)
                emp = CountrySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Country.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = CountrySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Country.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = CountrySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Country.objects.create(
                    country_name=data.get('country_name'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by')
                )
                serializer = CountrySerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveCountryApi(APIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Country.objects.filter(is_active=True), pk=pk)
                emp = CountrySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Country.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = CountrySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Country.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = CountrySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Country.objects.get(id=pk)
            queryset.country_name = data.get('country_name')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.save()
            serializer = CountrySerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Country.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class CityApi(APIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['city_name']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(City.objects.filter(is_active=True), pk=pk)
                emp = CitySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = City.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = CitySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = City.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = CitySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = City.objects.create(
                    city_name=data.get('city_name'),
                    country=data.get('country'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by')
                )
                serializer = CitySerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveCityApi(APIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(City.objects.filter(is_active=True), pk=pk)
                emp = CitySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = City.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = CitySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = City.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = CitySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = City.objects.get(id=pk)
            queryset.country = data.get('country')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.save()
            serializer = CitySerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = City.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class IndustryApi(APIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['industry_type']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Industry.objects.filter(is_active=True), pk=pk)
                emp = IndustrySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Industry.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = IndustrySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Industry.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = IndustrySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Industry.objects.create(
                    industry_type=data.get('industry_type'),
                    founded_in=data.get('founded_in'),
                    company_size=data.get('company_size'),
                    country=data.get('country'),
                    city=data.get('city'),
                    address=data.get('address'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by')


                )
                serializer = IndustrySerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " Industry Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveIndustryApi(APIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Industry.objects.filter(is_active=True), pk=pk)
                emp = IndustrySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Industry.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = IndustrySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Industry.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = IndustrySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Industry.objects.get(id=pk)
            queryset.industry_type = data.get('industry_type')
            queryset.founded_in = data.get('founded_in')
            queryset.company_size = data.get('company_size')
            queryset.country=data.get('country')
            queryset.city=data.get('city')
            queryset.address=data.get('address')
            queryset.create_by=data.get('create_by')
            queryset.update_by=data.get('update_by')

            queryset.save()
            serializer = IndustrySerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Industry.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class IndustryApi(APIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['industry_type']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Industry.objects.filter(is_active=True), pk=pk)
                emp = IndustrySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Industry.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = IndustrySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Industry.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = IndustrySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Industry.objects.create(
                    industry_type=data.get('industry_type'),
                    founded_in=data.get('founded_in'),
                    company_size=data.get('company_size'),
                    country=data.get('country'),
                    city=data.get('city'),
                    address=data.get('address'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by')


                )
                serializer = IndustrySerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " Industry Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveIndustryApi(APIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Industry.objects.filter(is_active=True), pk=pk)
                emp = IndustrySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Industry.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = IndustrySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Industry.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = IndustrySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Industry.objects.get(id=pk)
            queryset.industry_type = data.get('industry_type')
            queryset.founded_in = data.get('founded_in')
            queryset.company_size = data.get('company_size')
            queryset.country=data.get('country')
            queryset.city=data.get('city')
            queryset.address=data.get('address')
            queryset.create_by=data.get('create_by')
            queryset.update_by=data.get('update_by')
            
            queryset.save()
            serializer = IndustrySerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Industry.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class ApplicantStatusApi(APIView):
    queryset = ApplicantStatus.objects.all()
    serializer_class = ApplicantStatusSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['status_title']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(ApplicantStatus.objects.filter(is_active=True), pk=pk)
                emp = ApplicantStatusSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = ApplicantStatus.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ApplicantStatusSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = ApplicantStatus.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ApplicantStatusSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = ApplicantStatus.objects.create(
                    status_title=data.get('status_title'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    
                )
                serializer = ApplicantStatusSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " ApplicantStatus Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveIndustryApi(APIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Industry.objects.filter(is_active=True), pk=pk)
                emp = IndustrySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Industry.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = IndustrySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Industry.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = IndustrySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Industry.objects.get(id=pk)
            queryset.status_title = data.get('status_title')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
                        
            queryset.save()
            serializer = IndustrySerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Industry.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class ApplicantApi(APIView):
    queryset = Applicant.objects.all()
    serializer_class = ApplicantSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['status_title']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Applicant.objects.filter(is_active=True), pk=pk)
                emp = ApplicantSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Applicant.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ApplicantSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Applicant.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ApplicantSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Applicant.objects.create(
                    name=data.get('name'),
                    user=data.get('user'),
                    resume=data.get('resume'),
                    photo=data.get('photo'),
                    cover_photo=data.get('cover_photo'),
                    gender=data.get('gender'),
                    qualification=data.get('qualification'),
                    current_location=data.get('current_location'),
                    date_of_birth=data.get('date_of_birth'),
                    register_date=data.get('register_date'),
                    marital_status=data.get('marital_status'),
                    address=data.get('address'),
                    about=data.get('about'),
                    contact_no=data.get('contact_no'),
                    is_active=data.get('is_active'),
                    experience=data.get('experience'),
                    facebook_link=data.get('facebook_link'),
                    twitter_link=data.get('twitter_link'),
                    instagram_link=data.get('instagram_link'),
                    linkendin_link=data.get('linkendin_link'),
                    github_link=data.get('github_link'),
                    gitlab_link=data.get('gitlab_link'),
                    google_link=data.get('google_link'),
                    link1=data.get('link1'),
                    link2=data.get('link2'),
                    status=data.get('status'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by')
                    
                )
                serializer = ApplicantSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " Applicant Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveApplicantApi(APIView):
    queryset = Applicant.objects.all()
    serializer_class = ApplicantSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Applicant.objects.filter(is_active=True), pk=pk)
                emp = ApplicantSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Applicant.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ApplicantSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Applicant.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ApplicantSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Applicant.objects.get(id=pk)
            queryset.name = data.get('name')
            queryset.user = data.get('user')
            queryset.resume = data.get('resume')
            queryset.photo=data.get('photo')
            queryset.cover_photo=data.get('cover_photo')
            queryset.gender=data.get('gender')
            queryset.qualification=data.get('qualification')
            queryset.current_location=data.get('current_location')
            queryset.date_of_birth=data.get('date_of_birth')
            queryset.register_date=data.get('register_date')
            queryset.marital_status=data.get('marital_status')
            queryset.address=data.address('address')
            queryset.about=data.get('about')
            queryset.contact_no=data.get('contact_no')
            queryset.is_active=data.get('is_active')
            queryset.experience=data.get('experience')
            queryset.facebook_link=data.get('facebook_link')
            queryset.twitter_link=data.get('twitter_link')
            queryset.instagram_link=data.get('instagram_link')
            queryset.linkendin_link=data.get('linkendin_link')
            queryset.github_link=data.get('github_link')
            queryset.gitlab_link=data.get('gitlab_link')
            queryset.google_link=data.get('google_link')
            queryset.link1=data.get('link1')
            queryset.link2=data.get('link2')
            queryset.status=data.get('status')
            queryset.create_by=data.get('create_by')
            queryset.update_by=data.get('update_by')
            
                        
            queryset.save()
            serializer = ApplicantSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Applicant.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)