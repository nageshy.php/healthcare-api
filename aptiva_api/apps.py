from django.apps import AppConfig


class AptivaApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aptiva_api'
