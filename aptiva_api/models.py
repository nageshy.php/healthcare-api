from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models


def upload_resume(instance, filename):
    file_location = "resume/%s" % instance.resume
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


def upload_image(instance, filename):
    file_location = "%s" % instance.photo
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


def upload_file(instance, filename):
    file_location = "%s" % instance.file
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


def upload_audio(instance, filename):
    file_location = "%s" % instance.spoken_audio
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


class UpdateDetail(models.Model):
    """Update Table Detail model."""

    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    update_date_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        """Meta."""

        managed = True
        ordering = ["update_date_time"]

    def __str__(self):
        """__str__."""
        return str(self.user) + " || " + str(self.update_date_time)


class CreateDetail(models.Model):
    """Create Table Detail model."""

    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    create_date_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        """Meta."""

        managed = True
        ordering = ["create_date_time"]

    def __str__(self):
        """__str__."""
        return str(self.user) + " || " + str(self.create_date_time)


class Skills(models.Model):
    name = models.CharField(max_length=1000, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.name)


class Qualification(models.Model):
    degree = models.CharField(max_length=1000, null=True, blank=True)
    specialization = models.CharField(max_length=1000, null=True, blank=True)
    institute = models.CharField(max_length=1000, null=True, blank=True)
    university = models.CharField(max_length=1000, null=True, blank=True)
    year_of_passing = models.DateField(null=True, blank=True)
    percentage = models.CharField(max_length=100, null=True, blank=True)
    time_period = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.degree)


class Experience(models.Model):
    job_title = models.CharField(max_length=1000, null=True, blank=True)
    company = models.CharField(max_length=1000, null=True, blank=True)
    time_period = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.job_title)


class Country(models.Model):
    country_name = models.CharField(max_length=1000, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.country_name)


class City(models.Model):
    city_name = models.CharField(max_length=1000, null=True, blank=True)
    country = models.OneToOneField(Country, blank=True, on_delete=models.CASCADE)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.city_name)


class Industry(models.Model):
    industry_type = models.CharField(max_length=1000, null=True, blank=True)
    founded_in = models.CharField(max_length=1000, null=True, blank=True)
    company_size = models.CharField(max_length=1000, null=True, blank=True)
    country = models.ForeignKey(
        Country, null=True, blank=True, on_delete=models.SET_NULL)
    city = models.ForeignKey(
        City, null=True, blank=True, on_delete=models.SET_NULL)
    address = models.TextField(null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.industry_type)


class Admin(models.Model):
    MARITAL_CHOICES = (
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Other', 'Other')
    )
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Others', 'Others')
    )

    name = models.CharField(max_length=100, blank=True, null=True)
    phone_regex = RegexValidator(
        regex=r"^\+?1?\d{9,15}$",
        message="Phone number format: '+999999999'. Up to 15 digits allowed.",
    )
    phone_no = models.CharField(
        validators=[phone_regex], max_length=17, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=100, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    company = models.CharField(max_length=1000, null=True, blank=True)
    resume = models.FileField(upload_to=upload_resume, null=True, blank=True)
    photo = models.FileField(upload_to=upload_image, null=True, blank=True)
    cover_photo = models.FileField(
        upload_to=upload_image, null=True, blank=True)
    gender = models.CharField(
        max_length=10, choices=GENDER_CHOICES, null=True, blank=True)
    qualification = models.ManyToManyField(Qualification, blank=True)
    current_location = models.TextField(max_length=200, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    register_date = models.DateField(
        auto_now_add=True, null=True, editable=True, blank=True)
    marital_status = models.CharField(
        max_length=10, choices=MARITAL_CHOICES, null=True, blank=True)
    address = models.TextField(max_length=400, null=True, blank=True)
    about = models.TextField(null=True, blank=True)
    contact_no = models.CharField(max_length=17, blank=True)
    is_active = models.BooleanField(default=False)
    experience = models.ManyToManyField(Experience, blank=True)
    industry = models.ManyToManyField(Industry, blank=True)
    facebook_link = models.CharField(max_length=1000, null=True, blank=True)
    twitter_link = models.CharField(max_length=1000, null=True, blank=True)
    instagram_link = models.CharField(max_length=1000, null=True, blank=True)
    linkendin_link = models.CharField(max_length=1000, null=True, blank=True)
    github_link = models.CharField(max_length=1000, null=True, blank=True)
    gitlab_link = models.CharField(max_length=1000, null=True, blank=True)
    google_link = models.CharField(max_length=1000, null=True, blank=True)
    link1 = models.CharField(max_length=1000, null=True, blank=True)
    link2 = models.CharField(max_length=1000, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)

    class Meta:
        """Meta."""

        managed = True
        ordering = ["pk"]

    def __str__(self):
        """__str__."""
        return str(self.user) + " || " + str(self.name)


class ApplicantStatus(models.Model):
    status_title = models.CharField(max_length=100, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.status_title)


class Applicant(models.Model):
    MARITAL_CHOICES = (
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Other', 'Other')
    )
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Others', 'Others')
    )

    name = models.CharField(max_length=1000, null=True, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    resume = models.FileField(upload_to=upload_resume, null=True, blank=True)
    photo = models.FileField(upload_to=upload_image, null=True, blank=True)
    cover_photo = models.FileField(
        upload_to=upload_image, null=True, blank=True)
    gender = models.CharField(
        max_length=10, choices=GENDER_CHOICES, null=True, blank=True)
    qualification = models.ManyToManyField(Qualification, blank=True)
    current_location = models.TextField(max_length=200, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    register_date = models.DateField(
        auto_now_add=True, null=True, editable=True, blank=True)
    marital_status = models.CharField(
        max_length=10, choices=MARITAL_CHOICES, null=True, blank=True)
    address = models.TextField(max_length=400, null=True, blank=True)
    about = models.TextField(null=True, blank=True)
    contact_no = models.CharField(max_length=17, blank=True)
    is_active = models.BooleanField(default=False)
    experience = models.ManyToManyField(Experience, blank=True)
    facebook_link = models.CharField(max_length=1000, null=True, blank=True)
    twitter_link = models.CharField(max_length=1000, null=True, blank=True)
    instagram_link = models.CharField(max_length=1000, null=True, blank=True)
    linkendin_link = models.CharField(max_length=1000, null=True, blank=True)
    github_link = models.CharField(max_length=1000, null=True, blank=True)
    gitlab_link = models.CharField(max_length=1000, null=True, blank=True)
    google_link = models.CharField(max_length=1000, null=True, blank=True)
    link1 = models.CharField(max_length=1000, null=True, blank=True)
    link2 = models.CharField(max_length=1000, null=True, blank=True)
    status = models.ForeignKey(
        ApplicantStatus, null=True, blank=True, on_delete=models.SET_NULL)
    create_by = models.ForeignKey(
        CreateDetail, on_delete=models.SET_NULL, null=True, blank=True)
    update_by = models.ManyToManyField(UpdateDetail, blank=True)
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.user)
