from . import views
from django.urls import path

urlpatterns = [
    path('employee_list/', views.EmployeeListApi.as_view()),
    path('employee_details/<int:pk>', views.RetrieveEmployeeApi.as_view()),
    path('qualification/', views.QualificationtApi.as_view()),
    path('qualification_details/<int:pk>', views.RetrieveQualificationApi.as_view()),
    path('experience/', views.ExperienceApi.as_view()),
    path('experience_details/<int:pk>', views.RetrieveExperienceApi.as_view()),
    path('country/', views.CountryApi.as_view()),
    path('country_details/<int:pk>', views.RetrieveCountryApi.as_view()),
    path('city/', views.CityApi.as_view()),
    path('city_details/<int:pk>', views.RetrieveCityApi.as_view()),
    path('industry/', views.IndustryApi.as_view()),
    path('industry_details/<int:pk>', views.RetrieveIndustryApi.as_view()),
    path('applications_list/', views.ApplicantStatusApi.as_view()),
    path('application_details/<int:pk>', views.RetrieveIndustryApi.as_view()),
    path('applicant_list/', views.ApplicantApi.as_view()),
    path('applicant_details/<int:pk>', views.RetrieveApplicantApi.as_view()),
]
