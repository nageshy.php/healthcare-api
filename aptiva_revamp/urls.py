"""Aptiva URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import permissions
from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi
from aptiva_api import urls
# from jwtauth import urls

schema_view = get_schema_view(
    openapi.Info(
        title="Aptiva API",
        default_version='v1',
        description="Aptiva Developed by ....",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="nagesh@gmail.com"),
        urlconf='aptiva_api.urls'
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [

    re_path(r'^swagger(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0), name='schema-json'
            ),

    path('',
         schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'
         ),

    path('redoc/',
         schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'
         ),

    path('admin/',
         admin.site.urls
         ),

    re_path('aptiva/',
            include('aptiva_api.urls')
            ),
    re_path('aptiva_job/',
            include('aptiva_jobs.urls')
            )
    

]
