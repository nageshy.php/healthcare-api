from . import views
from django.urls import path, include

urlpatterns = [
    path('jobs/', views.jobs.as_view()),
    path('jobs_details/<int:pk>', views.RetrieveJobsApi.as_view()),
    path('categories/', views.categoryApi.as_view()),
    path('category_details/<int:pk>', views.RetrieveCategoryApi.as_view()),
    path('salaryRange/', views.SalaryRangeApi.as_view()),
    path('salaryRange_details/<int:pk>', views.RetrieveSalaryRangeApi.as_view()),
    path('JobStatus/', views.JobStatusApi.as_view()),
    path('JobStatus_details/<int:pk>', views.RetrieveJobStatusApi.as_view()),
    path('license/', views.LicenseApi.as_view()),
    path('license_details/<int:pk>', views.RetrieveLicenseApi.as_view()),
    path('applicant_jobs/', views.ApplicantAppliedJobsApi.as_view()),
    path('applicant_job_details/<int:pk>', views.RetrieveApplicantAppliedJobsApi.as_view()),
    path('favorite_jobs/', views.ApplicantFavouriteJobsApi.as_view()),
    path('favorite_job_details/<int:pk>', views.RetrieveFavouriteJobsApi.as_view()),
]

