from operator import is_
from rest_framework import response,status
from django.shortcuts import get_object_or_404
from .models import *
from .serializers import *
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models import Q
from rest_framework.views import APIView

class CustomPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data, status):
        return response.Response({
            'Links' : {
                'next' : self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count' : self.page.paginator.count,
            'limit' : self.page_size,
            'current_page' : self.page.number,
            'total_pages' : self.page.paginator.num_pages,
            'status_code' : status,
            'results'     : data
        })

class jobs(APIView):
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['job_title']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Jobs.objects.filter(is_active=True), pk=pk)
                emp = JobsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Jobs.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = JobsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Jobs.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = JobsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Jobs.objects.create(
                    job_title=data.get('job_title'),
                    job_address=data.get('job_address'),
                    job_description=data.get('job_description'),
                    experience=data.get('experience'),
                    career_level=data.get('career_level'),
                    broucher=data.get('broucher'),
                    logo=data.get('logo'),
                    banner=data.get('banner'),
                    duration=data.get('duration'),
                    shift=data.get('shift'),
                    shift_per_week=data.get('shift_per_week'),
                    scheduled_hours=data.get('scheduled_hours'),
                    latitude=data.get('latitude'),
                    longitude=data.get('longitude'),
                    is_active=data.get('is_active'),
                    country=data.get('country'),
                    city=data.get('city'),
                    category=data.get('category'),
                    employement_type=data.get('employement_type'),
                    create_by=data.get('create_by'),
                    status=data.get('status'),
                    ApplicantFavouriteJobs=data.get('ApplicantFavouriteJobs'),
                    pay_details = data.get('pay_details')


                )
                serializer = JobsSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveJobsApi(APIView):
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Jobs.objects.filter(is_active=True), pk=pk)
                emp = JobsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Jobs.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = JobsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Jobs.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = JobsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Jobs.objects.get(id=pk)
            queryset.erp_id = data.get('erp_id')
            queryset.mobile_number = data.get('mobile_number')
            queryset.Jobs_name_id = data.get('Admin_name_id')
            queryset.role_type_id = data.get('role_type_id')
            queryset.save()
            serializer = JobsSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Jobs.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class categoryApi(APIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['category_name']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Category.objects.filter(is_active=True), pk=pk)
                emp = CategorySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Category.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = CategorySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Category.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = CategorySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Category.objects.create(
                    category_name=data.get('category_name'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    is_active=data.get('is_active')

                )
                serializer = CategorySerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveCategoryApi(APIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Category.objects.filter(is_active=True), pk=pk)
                emp = CategorySerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Category.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = CategorySerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Category.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = CategorySerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Category.objects.get(id=pk)
            queryset.category_name = data.get('category_name')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.is_active = data.get('is_active')
            queryset.save()
            serializer = CategorySerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Category.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class SalaryRangeApi(APIView):
    queryset = SalaryRange.objects.all()
    serializer_class = SalaryRangeSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['salary_range']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(SalaryRange.objects.filter(is_active=True), pk=pk)
                emp = SalaryRangeSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = SalaryRange.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = SalaryRangeSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = SalaryRange.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = SalaryRangeSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = SalaryRange.objects.create(
                    salary_range=data.get('salary_range'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    is_active=data.get('is_active')

                )
                serializer = SalaryRangeSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveSalaryRangeApi(APIView):
    queryset = SalaryRange.objects.all()
    serializer_class = SalaryRangeSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(SalaryRange.objects.filter(is_active=True), pk=pk)
                emp = SalaryRangeSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = SalaryRange.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = SalaryRangeSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = SalaryRange.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = SalaryRangeSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = SalaryRange.objects.get(id=pk)
            queryset.salary_range = data.get('salary_range')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.is_active = data.get('is_active')
            queryset.save()
            serializer = SalaryRangeSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = SalaryRange.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class JobStatusApi(APIView):
    queryset = JobStatus.objects.all()
    serializer_class = JobStatusSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['salary_range']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(JobStatus.objects.filter(is_active=True), pk=pk)
                emp = JobStatusSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = JobStatus.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = JobStatusSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = JobStatus.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = JobStatusSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = JobStatus.objects.create(
                    status_title=data.get('status_title'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    is_active=data.get('is_active')

                )
                serializer = JobStatusSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveJobStatusApi(APIView):
    queryset = JobStatus.objects.all()
    serializer_class = JobStatusSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(JobStatus.objects.filter(is_active=True), pk=pk)
                emp = JobStatusSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = JobStatus.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = JobStatusSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = JobStatus.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = JobStatusSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = JobStatus.objects.get(id=pk)
            queryset.status_title = data.get('status_title')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.is_active = data.get('is_active')
            queryset.save()
            serializer = JobStatusSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = JobStatus.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class LicenseApi(APIView):
    queryset = License.objects.all()
    serializer_class = LicenseSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['license_name']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(License.objects.filter(is_active=True), pk=pk)
                emp = LicenseSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = License.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = LicenseSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = License.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = LicenseSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = License.objects.create(
                    license_name=data.get('license_name'),
                    license_id=data.get('license_id'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    is_active=data.get('is_active')

                )
                serializer = LicenseSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveLicenseApi(APIView):
    queryset = License.objects.all()
    serializer_class = LicenseSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(License.objects.filter(is_active=True), pk=pk)
                emp = LicenseSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = License.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = LicenseSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = License.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = LicenseSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = License.objects.get(id=pk)
            queryset.license_name = data.get('license_name')
            queryset.license_id = data.get('license_id')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.is_active = data.get('is_active')
            queryset.save()
            serializer = LicenseSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = License.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class PayDetailsApi(APIView):
    queryset = PayDetails.objects.all()
    serializer_class = PayDetailsSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['PayDetails_name']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(PayDetails.objects.filter(is_active=True), pk=pk)
                emp = PayDetailsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = PayDetails.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = PayDetailsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = PayDetails.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = PayDetailsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = PayDetails.objects.create(
                    hourly_pay=data.get('hourly_pay'),
                    weekly_pay=data.get('weekly_pay'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    is_active=data.get('is_active')

                )
                serializer = PayDetailsSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrievePayDetailsApi(APIView):
    queryset = PayDetails.objects.all()
    serializer_class = PayDetailsSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(PayDetails.objects.filter(is_active=True), pk=pk)
                emp = PayDetailsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = PayDetails.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = PayDetailsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = PayDetails.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = PayDetailsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = PayDetails.objects.get(id=pk)
            queryset.PayDetails_name = data.get('PayDetails_name')
            queryset.PayDetails_id = data.get('PayDetails_id')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.is_active = data.get('is_active')
            queryset.save()
            serializer = PayDetailsSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = PayDetails.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class ApplicantAppliedJobsApi(APIView):
    queryset = ApplicantAppliedJobs.objects.all()
    serializer_class = ApplicantAppliedJobsSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['applicant']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(ApplicantAppliedJobs.objects.filter(is_active=True), pk=pk)
                emp = ApplicantAppliedJobsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = ApplicantAppliedJobs.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ApplicantAppliedJobsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = ApplicantAppliedJobs.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ApplicantAppliedJobsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = ApplicantAppliedJobs.objects.create(
                    applicant=data.get('applicant'),
                    applied_job=data.get('applied_job'),
                    status=data.get('status'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    is_active=data.get('is_active')

                )
                serializer = ApplicantAppliedJobsSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveApplicantAppliedJobsApi(APIView):
    queryset = ApplicantAppliedJobs.objects.all()
    serializer_class = ApplicantAppliedJobsSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(ApplicantAppliedJobs.objects.filter(is_active=True), pk=pk)
                emp = ApplicantAppliedJobsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = ApplicantAppliedJobs.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ApplicantAppliedJobsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = ApplicantAppliedJobs.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ApplicantAppliedJobsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = ApplicantAppliedJobs.objects.get(id=pk)
            queryset.applicant = data.get('applicant')
            queryset.applied_job = data.get('applied_job')
            queryset.status = data.get('status')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.is_active = data.get('is_active')
            queryset.save()
            serializer = ApplicantAppliedJobsSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = ApplicantAppliedJobs.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class ApplicantFavouriteJobsApi(APIView):
    queryset = ApplicantFavouriteJobs.objects.all()
    serializer_class = ApplicantFavouriteJobsSerializer
    pagination_class = CustomPagination
    filter_backends = [SearchFilter]
    search_fields = ['applicant']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(ApplicantFavouriteJobs.objects.filter(is_active=True), pk=pk)
                emp = ApplicantFavouriteJobsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = ApplicantFavouriteJobs.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ApplicantFavouriteJobsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = ApplicantFavouriteJobs.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ApplicantFavouriteJobsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = ApplicantFavouriteJobs.objects.create(
                    applicant=data.get('applicant'),
                    favourite_job=data.get('favourite_job'),
                    wishlist_status=data.get('wishlist_status'),
                    create_by=data.get('create_by'),
                    update_by=data.get('update_by'),
                    is_active=data.get('is_active')

                )
                serializer = ApplicantFavouriteJobsSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

class RetrieveFavouriteJobsApi(APIView):
    queryset = ApplicantFavouriteJobs.objects.all()
    serializer_class = ApplicantFavouriteJobsSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(ApplicantFavouriteJobs.objects.filter(is_active=True), pk=pk)
                emp = ApplicantFavouriteJobsSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = ApplicantFavouriteJobs.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = ApplicantFavouriteJobsSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = ApplicantFavouriteJobs.objects.filter(is_active=True)
            page = paginator.paginate_queryset(queryset, request)
            serializer = ApplicantFavouriteJobsSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = ApplicantFavouriteJobs.objects.get(id=pk)
            queryset.applicant = data.get('applicant')
            queryset.favourite_job = data.get('favourite_job')
            queryset.wishlist_status = data.get('wishlist_status')
            queryset.create_by = data.get('create_by')
            queryset.update_by = data.get('update_by')
            queryset.is_active = data.get('is_active')
            queryset.save()
            serializer = ApplicantFavouriteJobsSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = ApplicantFavouriteJobs.objects.get(id=pk)
            queryset.update(is_active=False)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)