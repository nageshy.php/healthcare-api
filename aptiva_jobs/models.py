import re
from django.db import models
from django.contrib.auth.models import User

from aptiva_api.models import Applicant, ApplicantStatus, City, Country

# Create your models here.


def upload_resume(instance, filename):
    file_location = "resume/%s" % instance.resume
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


def upload_image(instance, filename):
    file_location = "%s" % instance.photo
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


def upload_file(instance, filename):
    file_location = "%s" % instance.file
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


def upload_audio(instance, filename):
    file_location = "%s" % instance.spoken_audio
    file_location = file_location.lower().replace(" ", "_")
    location = "%s/%s" % (file_location, filename)
    return location


class UpdateDetailUser(models.Model):
    """Update Table Detail model."""

    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    update_date_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        """Meta."""

        managed = True
        ordering = ["update_date_time"]

    def __str__(self):
        """__str__."""
        return str(self.user) + " || " + str(self.update_date_time)


class CreateDetailUser(models.Model):
    """Create Table Detail model."""

    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    create_date_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        """Meta."""

        managed = True
        ordering = ["create_date_time"]

    def __str__(self):
        """__str__."""
        return str(self.user) + " || " + str(self.create_date_time)


class Category(models.Model):
    category_name = models.CharField(max_length=1000, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='category_created_by')
    update_by = models.ManyToManyField(
        UpdateDetailUser, blank=True, related_name='category_updated_by')
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.category_name)


class EmployementType(models.Model):
    employement_name = models.CharField(max_length=1000, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='employement_type_created_by')
    update_by = models.ManyToManyField(
        UpdateDetailUser, blank=True, related_name='employement_type_updated_by')
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.employement_name)


class SalaryRange(models.Model):
    salary_range = models.CharField(max_length=1000, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='salary_range_created_by')
    update_by = models.ManyToManyField(
        UpdateDetailUser, blank=True, related_name='salary_range_updated_by')
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.salary_range)


class JobStatus(models.Model):
    status_title = models.CharField(max_length=100, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='job_status_created_by')
    update_by = models.ManyToManyField(
        UpdateDetailUser, blank=True, related_name='job_status_updated_by')
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.status_title)


class License(models.Model):
    license_name = models.CharField(max_length=100, null=True, blank=True)
    license_id = models.CharField(max_length=100, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='license_created_by')
    update_by = models.ManyToManyField(
        UpdateDetailUser, blank=True,related_name='license_updated_by')
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    expiry_date = models.DateField(null=True, blank=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.license_name)


class PayDetails(models.Model):
    hourly_pay = models.CharField(max_length=100, null=True, blank=True)
    weekly_pay = models.CharField(max_length=100, null=True, blank=True)
    bonus_pay = models.CharField(max_length=100, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='pay_details_created_by')
    update_by = models.ManyToManyField(
        UpdateDetailUser, blank=True, related_name='pay_details_updated_by')
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.hourly_pay)


class Jobs(models.Model):
    job_title = models.CharField(max_length=1000, null=True, blank=True)
    country = models.ForeignKey(
        Country, blank=True, null=True, on_delete=models.SET_NULL)
    city = models.ForeignKey(
        City, blank=True, null=True, on_delete=models.SET_NULL)
    job_address = models.TextField(blank=True, null=True)
    category = models.ForeignKey(
        Category, null=True, blank=True, on_delete=models.SET_NULL)
    job_description = models.TextField(blank=True, null=True)
    experience = models.CharField(max_length=100, null=True, blank=True)
    career_level = models.CharField(max_length=100, null=True, blank=True)
    employement_type = models.ForeignKey(
        EmployementType, null=True, blank=True, on_delete=models.SET_NULL)
    broucher = models.FileField(upload_to=upload_resume, null=True, blank=True)
    logo = models.FileField(upload_to=upload_image, null=True, blank=True)
    banner = models.FileField(
        upload_to=upload_image, null=True, blank=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='jobs_created_by')
    update_by = models.ManyToManyField(
        UpdateDetailUser, blank=True,related_name='jobs_updated_by')
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    status = models.ForeignKey(
        JobStatus, blank=True, null=True, on_delete=models.SET_NULL)
    license = models.ForeignKey(
        License, blank=True, null=True, on_delete=models.SET_NULL)
    duration = models.CharField(max_length=100, null=True, blank=True)
    shift = models.CharField(max_length=100, null=True, blank=True)
    shift_per_week = models.CharField(max_length=100, null=True, blank=True)
    scheduled_hours = models.CharField(max_length=100, null=True, blank=True)
    pay_details = models.ForeignKey(
        PayDetails, blank=True, null=True, on_delete=models.SET_NULL)
    latitude = models.CharField(max_length=1000, null=True, blank=True)
    longitude = models.CharField(max_length=1000, null=True, blank=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.job_title)


class ApplicantAppliedJobs(models.Model):
    applicant = models.ForeignKey(
        Applicant, blank=True, null=True, on_delete=models.SET_NULL)
    applied_job = models.ForeignKey(
        Jobs, blank=True, null=True, on_delete=models.SET_NULL)
    status = models.ForeignKey(
        ApplicantStatus, blank=True, null=True, on_delete=models.SET_NULL)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name="applicant_applied_created_by")
    update_by = models.ManyToManyField(UpdateDetailUser, blank=True,related_name="applicant_applied_updated_by")
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.applicant) + " || " + str(self.applied_job)


class ApplicantFavouriteJobs(models.Model):
    applicant = models.ForeignKey(
        Applicant, blank=True, null=True, on_delete=models.SET_NULL)
    favourite_job = models.ForeignKey(
        Jobs, blank=True, null=True, on_delete=models.SET_NULL)
    wishlist_status = models.BooleanField(default=True)
    create_by = models.ForeignKey(
        CreateDetailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name="applicant_favourite_created_by")
    update_by = models.ManyToManyField(UpdateDetailUser, blank=True,related_name="applicant_favourite_updated_by")
    created_date = models.DateField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateField(auto_now=True, null=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']

    def __str__(self):
        return str(self.applicant) + " || " + str(self.favourite_job)
