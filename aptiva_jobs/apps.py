from django.apps import AppConfig


class AptivaJobsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aptiva_jobs'
