from .models import *
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class EmployementTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployementType
        fields = '__all__'


class SalaryRangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalaryRange
        fields = '__all__'


class JobStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobStatus
        fields = '__all__'


class LicenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = License
        fields = '__all__'


class PayDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayDetails
        fields = '__all__'


class JobsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Jobs
        fields = '__all__'


class ApplicantAppliedJobsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicantAppliedJobs
        fields = '__all__'


class ApplicantFavouriteJobsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicantFavouriteJobs
        fields = '__all__'
